﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.Domain
{
    class RoundTest
    {
        private List<Category> _categories;
        IRepository<Category, int> repoCategory = new CategoryRepositoryFromFile();
        Round round;
        Round round2;
        [SetUp]
        public void Setup()
        {
            _categories = repoCategory.GetAll();
            round = new Round(_categories);
            round2 = new Round(_categories);
        }

        [Test]
        public void RoundShouldHave5Categories()
        {
            Assert.AreEqual(5, round.Categories.Count);
        }

        [Test]
        public void RoundShouldHaveIdBetween0And1000()
        {
            Assert.NotNull(round.Id);
            Assert.IsTrue(round.Id > 0);
            Assert.IsTrue(round.Id < 1000);
        }

        [Test]
        public void RoundShouldHaveALetter()
        {
            Assert.NotNull(round.Letter);
        }

        [Test]
        public void ShouldResetLetter()
        {
            round.ResetLetter();
            Assert.AreEqual(' ', round.Letter);
        }
        [Test]
        public void RoundShouldHaveTwoTurnsMax()
        {
            RoundAnswersDTO dto = Substitute.For<RoundAnswersDTO>();

            round.SetDataToTurn(dto);
            round.SetDataToTurn(dto);
            round.SetDataToTurn(dto);
            round.SetDataToTurn(dto);

            Assert.AreEqual(2, round.Turns.Count);
        }


        [Test]
        public void RoundIsCompletedWhenBothTurnsIsFinish()
        {
            Turn turn = new Turn(round2.Id);
            Turn turn2 = new Turn(round2.Id);
            turn.IsFinished = true;
            turn2.IsFinished = true;

            round2.Turns.Add(turn);
            round2.Turns.Add(turn2);


            bool RoundIsCompleted = round2.ValidateRoundCompleted();

            Assert.IsTrue(RoundIsCompleted);
        }

        [Test]
        public void WinnerOfRoundShouldReturnMinusOneWherNobodyWin()
        {
            round.WinnerOfRound(Substitute.For<apiExample.Model.Session>());

            Assert.AreEqual(-1, round.IdWinnerOfRound);
        }

    }
}
