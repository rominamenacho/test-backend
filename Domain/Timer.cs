﻿using NUnit.Framework;

namespace Tests.Domain
{
    class Timer
    {

        apiExample.Model.Timer timer = new apiExample.Model.Timer();

        [Test]
        public void Init_Time_Never_Be_Negative()
        {
            Assert.AreNotEqual(0, timer.InitTime);
        }


        [Test]
        public void Timer_Never_Be_More_Than_60_Seconds()
        {
            timer.TotalTime = 90.0f;
            timer.RecalculateTimer();

            Assert.AreEqual(60.0f, timer.TotalTime);
        }

        [Test]
        public void Timer_Never_Be_Negative()
        {
            timer.TotalTime = -90.0f;
            timer.RecalculateTimer();

            Assert.AreEqual(30.0f, timer.TotalTime);
        }
    }
}
