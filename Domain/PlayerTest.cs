﻿using apiExample.Model;
using NUnit.Framework;

namespace Tests.Domain
{
    class PlayerTest
    {

        Player player;
        Session session;

        [SetUp]
        public void Setup()
        {
            session = new Session();
            player = new Player("juan", "juan@gmail.com");
        }

        [Test]
        public void PlayerShouldHaveAnIdBetweenOand10000()
        {

            Player playerIdTest = new Player("pepe", "pepe@gmail.com");

            Assert.IsTrue(playerIdTest.Id > 0);
            Assert.IsTrue(playerIdTest.Id < 10000);
        }

        [Test]
        public void PlayerShouldHaveAnName()
        {

            Player playerIdTest = new Player("pepe", "pepe@gmail.com");

            Assert.IsNotNull(playerIdTest.Name != null);
        }

        [Test]
        public void PlayerShouldHaveAnEmail()
        {

            Player playerIdTest = new Player("pepe", "pepe@gmail.com");
            Assert.IsNotNull(playerIdTest.Email != null);
        }

        [Test]
        public void AlTheBeginingPlayerIsNotLookingForPlay()
        {

            Player playerIdTest = new Player("pepe", "pepe@gmail.com");
            Assert.IsFalse(playerIdTest.IslookingForPlay);
        }



        [Test]
        public void PlayerShouldHaveASessionList()
        {

            player.AssignSession(session);

            Assert.AreNotEqual(0, player.SessionGameList.Count);
        }
        [Test]
        public void PlayerShouldHaveDifferentsSessionInSessionList()
        {

            player.AssignSession(session);
            Session sessionTwo = new Session();
            player.AssignSession(sessionTwo);

            Assert.AreEqual(2, player.SessionGameList.Count);
        }

        [Test]
        public void PlayerDontShouldAssignSameSessionToSessionList()
        {

            player.AssignSession(session);
            player.AssignSession(session);

            Assert.AreEqual(1, player.SessionGameList.Count);
        }

        [Test]
        public void ANewPlayerShouldHaveAnIdDifferentOfZero()
        {
            Assert.AreNotEqual(0, player.Id);
        }

        [Test]
        public void WhenWinASessionShouldHaveOneMoreVictory()
        {

            int victory = player.Victories;
            player.AddVictory();

            Assert.AreEqual(victory + 1, player.Victories);

        }

        [Test]
        public void WhenWinASessionShouldHave100CoinsMore()
        {

            int coins = player.Wallet.Coins + 100;
            player.GiveCoins();

            Assert.AreEqual(coins, player.Wallet.Coins);

        }

    }
}
