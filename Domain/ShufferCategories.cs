﻿using apiExample.Model;
using apiExample.Repository;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace Tests.Domain
{


    class ShufferCategories
    {
        IRepository<Category, int> repoCategory = new CategoryRepositoryFromFile();
        [Test]
        public void RepositoryCategoriesShouldReturnFiveCategories()
        {

            List<Category> categories = repoCategory.GetAll();
            Assert.AreEqual(5, categories.Count);

        }


        [Test]
        public void ShufferCategoriesShouldReturnFiveCategories()
        {

            ShufferStub stub = new ShufferStub();
            List<Category> listAfterShuffer = stub.ShuffleCategories(stub.categories);
            Assert.AreEqual(5, listAfterShuffer.Count);
        }
    }

    class ShufferStub : apiExample.Repository.ShufferCategories
    {
        public List<Category> categories;

        public ShufferStub()
        {
            categories = fill();
        }
        private List<Category> fill()
        {

            var json = File.ReadAllText(@"C:\Users\54926\source\repos\apiExample\Repository\Category\ES_categorias.json");
            return JsonConvert.DeserializeObject<List<Category>>(json);
        }


    }
}
