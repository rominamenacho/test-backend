﻿using apiExample.Model;
using apiExample.Repository;
using apiExample.UseCase;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.Domain
{
    class TurnTest
    {
        apiExample.Model.Turn turn;
        List<string> answerList;
        apiExample.Model.Session session;
        MapperPlayer playerService;
        [SetUp]
        public void Setup()
        {
            IRepositoryPlayer repoPlayer = Substitute.For<PlayerRepositoryInMemory>();
            playerService = new MapperPlayer();
            session = new apiExample.Model.Session();

            List<Category> categories;
            categories = SetupCategories();

            IRound round = Substitute.For<apiExample.Model.Round>(categories);

            turn = new apiExample.Model.Turn(round.Id);
            answerList = GetAnswersListTest();
        }


        [Test]
        public void AnswersListShouldBeFilledAfterPlayARound()
        {

            turn.SetAnswers(answerList);
            Assert.AreEqual(5, turn.Answers.Count);
        }

        [Test]
        public void AnswersListShouldntBeNullAfterPlayARound()
        {

            turn.SetAnswers(answerList);
            Assert.IsNotNull(turn.Answers);
        }



        [Test]
        public void AnswersListShouldBeEmptyAfterCleanRoundData()
        {
            turn.SetAnswers(answerList);
            turn.Answers.Clear();
            Assert.AreEqual(0, turn.Answers.Count);
        }

        [Test]
        public void CheckAnswerShouldReturnAListWithCorrectAnswersAndCorrectCount()
        {

            turn.SetAnswers(answerList);
            Assert.AreEqual(0, turn.CorrectAnswersCount);
        }

        [Test]
        public void ScoreShouldNotHasANegativeValue()
        {
            turn.SetScore();
            Assert.AreNotEqual(-1, turn.CorrectAnswersCount);
        }

        [Test]
        public void ScoreShouldBeZeroAfterReset()
        {
            turn.SetScore();
            turn.ResetScore();
            Assert.AreEqual(0, turn.CorrectAnswersCount);
        }
        //[Test]
        //public void CheckAnswerShouldReturnAListWithFiveAnswers()
        //{

        //    turn.SetAnswers(answerList);
        //    AnswersCheckedDTO dto = turn.CheckAnswers();

        //    Assert.AreEqual(5, dto.Answers.Count);
        //}
        //[Test]
        //public void CheckAnswerShouldReturnCorrectCountAnswers()
        //{

        //    turn.SetAnswers(answerList);
        //    AnswersCheckedDTO dto = turn.CheckAnswers();

        //    Assert.IsNotNull(dto.CorrectCount);
        //}
        [Test]
        public void ShouldHaveRoundId()
        {
            turn.SetAnswers(answerList);
            Assert.IsNotNull(turn.MyRoundId);
        }
        [Test]
        public void ShouldHavePlayerId()
        {
            turn.SetAnswers(answerList);
            Assert.IsNotNull(turn.PlayerId);
        }



        //[Test]
        //public void FinishedStateShouldBeTrueAfterCheckAnswers()
        //{

        //    turn.SetAnswers(answerList);
        //    turn.CheckAnswers();
        //    Assert.AreEqual(true, turn.IsFinished);
        //}


        private List<string> GetAnswersListTest()
        {
            List<string> answerList = new List<string>();
            answerList.Add("01");
            answerList.Add("02");
            answerList.Add("02");
            answerList.Add("02");
            answerList.Add("02");
            return answerList;
        }
        private List<Category> SetupCategories()
        {
            CategoryRepositoryFromFile repo = Substitute.For<CategoryRepositoryFromFile>();
            return repo.GetAll();
        }

    }
}
