﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.Domain
{
    class SessionTest
    {

        Session session;
        IRound round;
        List<Category> categories;
        IRepositoryPlayer repoPlayer;
        IRepositorySession repoSession;
        MapperPlayer _mapperPlayer;
        MapperSession _mapperSession;
        Player p1;
        Player p2;

        [SetUp]
        public void Setup()
        {
            _mapperPlayer = new MapperPlayer();
            _mapperSession = new MapperSession();
            repoSession = new SessionRepositoryInMemory();
            repoPlayer = new PlayerRepositoryInMemory();
            categories = SetupCategories();

            session = new Session();
            round = new Round(categories);

            session.AddRoundToSession(round);
            repoSession.Insert(_mapperSession.FromSessionToDto(session));



            p1 = new Player("john", "john@dsf.com");
            p1.SessionGameList.Add(session);
            p2 = new Player("peter", "peter@gmail.com");
            p2.SessionGameList.Add(session);
            repoPlayer.Insert(_mapperPlayer.FromPlayerToDbDTO(p1));
            repoPlayer.Insert(_mapperPlayer.FromPlayerToDbDTO(p2));
        }
        private List<Category> SetupCategories()
        {
            CategoryRepositoryFromFile repo = Substitute.For<CategoryRepositoryFromFile>();
            return repo.GetAll();
        }
        [Test]
        public void SessionShouldHaveDifferentsRoundsInRoundList()
        {
            PlayCompleteTurn(round);

            IRound round2 = new Round(categories);
            session = _mapperSession.FromDtoToSession(repoSession.GetById(session.Id));
            session.AddRoundToSession(round2);
            PlayCompleteTurn(round2);

            Assert.AreEqual(2, session.Rounds.Count);
        }

        private void PlayCompleteTurn(IRound _round)
        {


            RoundAnswersDTO dto = GetRoundAnswersDTO(p1.Id, _round);
            RoundAnswersDTO dto2 = GetRoundAnswersDTO(p2.Id, _round);

            CheckAnswersUseCase useCase = new CheckAnswersUseCase(repoSession, repoPlayer);
            useCase.Execute(dto);
            useCase.Execute(dto2);

        }

        [Test]
        public void SessionShouldNotAssignSameRoundToRoundList()
        {

            session.AddRoundToSession(round);
            session.AddRoundToSession(round);

            Assert.AreEqual(1, session.Rounds.Count);
        }

        [Test]
        public void ShouldntAddNewRoundIfPrevIsntFinished()
        {
            PlayCompleteTurn(round);
            session = _mapperSession.FromDtoToSession(repoSession.GetById(session.Id));


            IRound round2 = new Round(categories);
            session.AddRoundToSession(round2);

            IRound round3 = new Round(categories);
            session.AddRoundToSession(round3);


            Assert.AreEqual(2, session.Rounds.Count);
        }


        private RoundAnswersDTO GetRoundAnswersDTO(int playerId, IRound _round)
        {
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.Answers = GetAnswersListTest();
            dto.PlayerID = playerId;
            dto.RoundID = _round.Id;
            dto.SessionID = session.Id;
            return dto;
        }

        private List<string> GetAnswersListTest()
        {
            List<string> answerList = new List<string>();
            answerList.Add("01");
            answerList.Add("02");
            answerList.Add("03");
            answerList.Add("04");
            answerList.Add("05");
            return answerList;
        }
    }
}
