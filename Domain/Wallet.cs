﻿using NUnit.Framework;

namespace Tests.Domain
{
    class Wallet
    {


        [Test]
        public void ShouldIncrease100CoinsEachTime()
        {
            apiExample.Model.Wallet wallet = new apiExample.Model.Wallet();
            int coins = wallet.Coins + 100;
            wallet.SetCoins(100);

            Assert.AreEqual(coins, wallet.Coins);

        }
    }
}
