﻿using apiExample.DTO;
using apiExample.Repository;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.Repository
{
    class FindPlayersInMemory
    {
        IRepositoryPlayer repo = new PlayerRepositoryInMemory();

        [Test]
        public void GetAllShouldReturnMoreThanOneResult()
        {

            List<PlayerDbDTO> results = repo.GetAll();
            Assert.IsTrue(results.Count > 1);
        }

        [Test]
        public void GetByIdShouldReturnOnePlayer()
        {
            int id = repo.GetByEmail("nico@gmail.com").Id;
            PlayerDbDTO results = repo.GetById(id);
            Assert.IsTrue(results != null);
        }

        [Test]
        public void GetByEmailShouldReturnOnePlayer()
        {

            PlayerDbDTO results = repo.GetByEmail("nico@gmail.com");
            Assert.IsTrue(results != null);
        }
    }
}
