﻿using apiExample.Model;
using apiExample.Repository;
using apiExample.UseCase;
using NUnit.Framework;
using System;

namespace Tests.UseCase
{
    class SetdailyRewardsUseCaseTest
    {
        private IRepositoryPlayer _repositoryPlayer;
        SetDailyRewardUseCase useCase;
        MapperPlayer mapperPLayer;

        [SetUp]
        public void SetUp()
        {
            _repositoryPlayer = new PlayerRepositoryInMemory();

            mapperPLayer = new MapperPlayer();
        }

        [Test]
        public void ShouldIncrease100CoinsToPlayer()
        {

            Player playerTest = new Player("test", "t4est@gmail.com");
            playerTest.LoginDate = playerTest.LoginDate.AddDays(-10);
            _repositoryPlayer.Insert(mapperPLayer.FromPlayerToDbDTO(playerTest));
            int coinsBefore = playerTest.Wallet.Coins;
            useCase = new SetDailyRewardUseCase(_repositoryPlayer);
            string response = useCase.SetReward(playerTest.Id);

            Assert.AreEqual("Reclamo sus recompensas con exito", response);
            Assert.AreEqual(coinsBefore + 100, _repositoryPlayer.GetById(playerTest.Id).Wallet.Coins);

        }

        [Test]
        public void ShouldUpdateDateTimeToPlayer()
        {

            Player playerTest = new Player("test", "t4est@gmail.com");
            playerTest.LoginDate = DateTime.Now.AddDays(-1);
            var yesterday = playerTest.LoginDate;
            _repositoryPlayer.Insert(mapperPLayer.FromPlayerToDbDTO(playerTest));

            useCase = new SetDailyRewardUseCase(_repositoryPlayer);
            useCase.SetReward(playerTest.Id);

            Assert.AreEqual(DateTime.Now.Day, _repositoryPlayer.GetById(playerTest.Id).LoginDate.Day);

        }
        [Test]
        public void ShouldThrowExceptionIfNull()
        {
            string response = useCase.SetReward(-400);
            Assert.AreEqual("No se encontro al player. Object reference not set to an instance of an object.", response);
        }


        //[Test]
        //public void ShouldThrowExceptionIsadasdadasdsa()
        //{
        //    Player playerTest = new Player("test", "t4est@gmail.com");
        //    _repositoryPlayer.Insert(playerTest);


        //    SetDailyRewardUseCase useCase2 = new SetDailyRewardUseCase(new PlayerRepositoryInMemory());
        //    string response = useCase2.SetReward(playerTest.Id);

        //    Assert.AreEqual("No se encontro al player", response);
        //}
    }
}
/*
  //var ex = Assert.Throws<HttpResponseException>(() => usecase.FindByIdAndBySessionId(roundId, sessionId));
            //  Assert.AreEqual("Not Found", ex.Message);
            Assert.Throws<HttpResponseException>(() => usecase.FindByIdAndBySessionId(roundId, sessionId));
 
 */