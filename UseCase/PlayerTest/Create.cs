﻿//namespace Tests.UseCase
//{
//  class Create
// {
//    [Test]
//    public void ShouldInsertANewPlayerIntoRepository()
//    {

//        var playerFromRepo = new Mock<Player>();
//        var mockPlayerRepo = new Mock<IRepositoryPlayer>();

//        mockPlayerRepo.Setup(m => m.Insert(It.IsAny<PlayerDbDTO>()));

//        CreatePlayerUseCase useCase = new CreatePlayerUseCase(mockPlayerRepo.Object);
//        PlayerDTO dto = new PlayerDTO();
//        dto.Name = "Ro";
//        dto.Email = "hola@gmail.com";

//        PlayerDbDTO playerCreated = new PlayerDbDTO();
//        playerCreated = useCase.Create(dto);

//        Assert.AreEqual("Ro", playerCreated.Name);
//        Assert.AreEqual("hola@gmail.com", playerCreated.Email);
//        Assert.IsNotNull(playerCreated.Id);
//        Assert.IsNotNull(playerCreated);
//    }
//}
//}
/*
 * 
 * // Creamos el mock sobre nuestra interfaz
var mock = new Mock<IFoo>();
 
// Definimos el comportamiento del método GetCount y su resultado
mock.Setup(m => m.GetCount()).Returns(1);
 
// Creamos una instancia del objeto mockeado y la testeamos
Assert.AreEqual(1, mock.Object.GetCount());
 mock.Setup(m => m.ToUpperCase(It.IsAny<string>()))
.Returns((string value) => { return value.ToUpperInvariant(); });


 var mockPersonRepository = new Mock<IPersonRepository>();
 
 Simulamos un comportamiento correcto
mockPersonRepository.Setup(m => m.Update(It.IsAny<Person>())).Returns(true);
 
// Simulamos un comportamiento incorrecto
mockPersonRepository
.Setup(m => m.Create(It.Is<Person>(p => p.Age > 0)).Returns(false);
 
// Creamos una instancia del mock y la inyectamos a la capa superior
var personService = new PersonService(mockPersonRepository.Object);
 
// Probamos
Assert.IsTrue(personService.Update(new Person()));
Assert.IsFalse(personService.Create(new Person { Age = -1 }));
 
 */