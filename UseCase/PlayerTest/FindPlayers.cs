﻿using apiExample.DTO;
using apiExample.Repository;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.UseCase
{
    class FindPlayers
    {
        IRepositoryPlayer repo = new PlayerRepositoryInMemory();
        FindPlayersUseCase useCase;

        [SetUp]
        public void Setup()
        {
            useCase = new FindPlayersUseCase(repo);
        }



        [Test]
        public void GetAllShouldReturnMoreThanOneResult()
        {

            List<PlayerDbDTO> results = useCase.GetAll();
            Assert.IsTrue(results.Count > 1);
        }

        [Test]
        public void GetByIdShouldReturnOnePlayer()
        {
            int id = repo.GetByEmail("nico@gmail.com").Id;

            PlayerDbDTO results = useCase.GetPlayerById(id);
            Assert.IsTrue(results != null);
        }

        [Test]
        public void GetByEmailShouldReturnOnePlayer()
        {

            PlayerDbDTO results = useCase.GetByEmail("nico@gmail.com");
            Assert.IsTrue(results != null);
        }
    }
}
