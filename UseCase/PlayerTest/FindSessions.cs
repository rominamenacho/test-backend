﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;

namespace Tests.UseCase
{
    class FindSessions
    {
        IRepositoryPlayer repository = new PlayerRepositoryFireBase();
        FindSessionsPlayerUseCase useCase;
        PlayerDbDTO player;
        MapperPlayer playerMapper;

        [SetUp]
        public void SetUp()
        {
            useCase = new FindSessionsPlayerUseCase(repository);
            player = repository.GetByEmail("mumi2@gmail.com");
            playerMapper = new MapperPlayer();
            SetDataToPlayer(playerMapper.FromPlayerDTO(player));

        }

        private void SetDataToPlayer(Player player)
        {
            apiExample.Model.Session session = new apiExample.Model.Session();
            player.AssignSession(session);
        }

        [Test]
        public void ShouldReturnAListOfSessionsData()
        {


            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);
            Assert.IsTrue(response.Count > 0);

        }

        [Test]
        public void AllSessionInListHaveAnIdSession()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containIdSession = new List<bool>();
            foreach (var item in response)
            {
                if (item.IdSession != 0) containIdSession.Add(true);
            }
            Assert.IsTrue(containIdSession.Count == response.Count);

        }
        [Test]
        public void AllSessionInListHaveAnIdPlayer()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containIdPlayer = new List<bool>();
            foreach (var item in response)
            {
                if (item.IdPlayer != 0) containIdPlayer.Add(true);
            }
            Assert.IsTrue(containIdPlayer.Count == response.Count);

        }

        [Test]
        public void AllSessionInListHaveAnIdOpponent()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containIdOpponent = new List<bool>();
            foreach (var item in response)
            {
                if (item.IdOpponent != 0) containIdOpponent.Add(true);
            }
            Assert.IsTrue(containIdOpponent.Count == response.Count);

        }

        [Test]
        public void AllSessionInListHaveAnIdRound()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containIdRound = new List<bool>();
            foreach (var item in response)
            {
                if (item.IdRound != 0) containIdRound.Add(true);
            }
            Assert.IsTrue(containIdRound.Count == response.Count);

        }

        [Test]
        public void AllSessionInListHaveAnOpponentName()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containOpponentName = new List<bool>();
            foreach (var item in response)
            {
                if (item.OpponentName.Length > 0) containOpponentName.Add(true);
            }
            Assert.IsTrue(containOpponentName.Count == response.Count);

        }

        [Test]
        public void AllSessionInListShouldHaveAnResult()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containResult = new List<bool>();

            foreach (var dto in response)
            {
                if (dto.Result[0].LastIndexOf(1) != 0 || dto.Result[1].LastIndexOf(1) != 0) containResult.Add(true);
            }
            Assert.IsTrue(containResult.Count == response.Count);

        }

        [Test]
        public void AllSessionInListShouldHavePlayerIds()
        {

            List<InfoSessionListDTO> response = useCase.GetInfoSessionList(player);

            List<bool> containResult = new List<bool>();

            foreach (var dto in response)
            {
                if (dto.Result[0].LastIndexOf(0) != 0 && dto.Result[1].LastIndexOf(0) != 0) containResult.Add(true);
            }
            Assert.IsTrue(containResult.Count == response.Count);

        }

    }
}
