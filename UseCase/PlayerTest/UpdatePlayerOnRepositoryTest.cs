﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests.UseCase.PlayerTest
{
    public class UpdatePlayerOnRepositoryTest
    {

        IRepositoryPlayer _repoPlayer = new PlayerRepositoryInMemory();
        UpdatePlayerOnRepositoryUseCase _playerUseCase;

        Session session;
        IRound round;
        Player player1;
        Player player2;
        MapperPlayer playerMapper;

        [SetUp]
        public void SetUp()
        {
            _playerUseCase = new UpdatePlayerOnRepositoryUseCase(_repoPlayer);
            playerMapper = new MapperPlayer();
            CreateEnviroment();

        }

        [Test]
        public void AfterUpdatedIsInProcessValueShouldBeEqualToSession()
        {

            var valueInPlayer = player1.SessionGameList[0].IsInProcess;

            _playerUseCase.UpdatePlayerProperties(CreateRoundAnswersDTOForTests(player1.Id), session, round, player1);
            Assert.AreEqual(session.IsInProcess, valueInPlayer);
        }


        private RoundAnswersDTO CreateRoundAnswersDTOForTests(int player1Id)
        {
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.PlayerID = player1Id;
            dto.RoundID = round.Id;
            dto.SessionID = session.Id;
            string oneCorrectAnswer = round.Categories.FirstOrDefault().Words.Where(a => a.StartsWith(round.Letter.ToString())).FirstOrDefault();
            dto.Answers = new List<string>() { oneCorrectAnswer, "-", "-", "-", "-" };
            return dto;
        }

        private void CreateEnviroment()
        {
            IRepository<Category, int> repoCategory = new CategoryRepositoryFromFile();

            session = new apiExample.Model.Session();
            round = new apiExample.Model.Round(repoCategory.GetAll());
            session.AddRoundToSession(round);

            player1 = new Player("john", "john@dsf.com");
            player2 = new Player("peter", "peter@gmail.com");
            player1.AssignSession(session);
            player2.AssignSession(session);

            _repoPlayer.Insert(playerMapper.FromPlayerToDbDTO(player1));
            _repoPlayer.Insert(playerMapper.FromPlayerToDbDTO(player2));

            MapperSession mapperSession = new MapperSession();
            IRepositorySession _repoSession = new SessionRepositoryInMemory();
            _repoSession.Insert(mapperSession.FromSessionToDto(session));
        }

    }
}
