﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests.UseCase
{
    class GetRoundResultTest
    {
        IRepositoryPlayer repositoryPlayer;
        IRepositorySession repositorySession;
        MapperPlayer playerService;
        GetRoundResultUseCase useCase;
        apiExample.Model.Session session;
        IRound round;
        Player player1, player2;

        [SetUp]
        public void Setup()
        {
            repositoryPlayer = new PlayerRepositoryInMemory();
            repositorySession = new SessionRepositoryInMemory();

            playerService = new MapperPlayer();

            CreateEnviroment();
            useCase = new GetRoundResultUseCase(repositoryPlayer, repositorySession);
        }


        [Test]
        public void ShouldReturnADTOWithListOfCategories()
        {

            RoundResultDTO response = useCase.GetRoundResult(round.Id, session.Id, player1.Id);
            Assert.IsNotNull(response.Categories);

        }
        [Test]
        public void ShouldReturnADTOWithListOfTurnsInRound()
        {

            RoundResultDTO response = useCase.GetRoundResult(round.Id, session.Id, player1.Id);
            Assert.IsNotNull(response.Turns);

        }

        [Test]
        public void ShouldReturnADTOWithOpponentName()
        {

            RoundResultDTO response = useCase.GetRoundResult(round.Id, session.Id, player1.Id);
            Assert.IsNotNull(response.OpponentName);
            Assert.IsTrue(response.OpponentName.Length > 0);

        }

        [Test]
        public void ShouldReturnADTOWithWinnerName()
        {

            RoundResultDTO response = useCase.GetRoundResult(round.Id, session.Id, player1.Id);
            Assert.IsNotNull(response.WinnerName);
            Assert.IsTrue(response.WinnerName.Length > 0);

        }

        private void CreateEnviroment()
        {
            IRepository<Category, int> repoCategory = new CategoryRepositoryFromFile();

            session = new apiExample.Model.Session();
            round = new apiExample.Model.Round(repoCategory.GetAll());
            session.AddRoundToSession(round);

            player1 = new Player("john", "john@dsf.com");
            player2 = new Player("peter", "peter@gmail.com");
            player1.AssignSession(session);
            player2.AssignSession(session);

            MapperPlayer playerMapper = new MapperPlayer();

            repositoryPlayer.Insert(playerMapper.FromPlayerToDbDTO(player1));
            repositoryPlayer.Insert(playerMapper.FromPlayerToDbDTO(player2));


            MapperSession mapperSession = new MapperSession();
            repositorySession.Insert(mapperSession.FromSessionToDto(session));

            CheckAnswersUseCase addAnswers = new CheckAnswersUseCase(repositorySession, repositoryPlayer);
            addAnswers.Execute(CreateRoundAnswersDTOForTests(player1.Id));
            addAnswers.Execute(CreateRoundAnswersDTOForTests(player2.Id));
        }

        private RoundAnswersDTO CreateRoundAnswersDTOForTests(int player1Id)
        {
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.PlayerID = player1Id;
            dto.RoundID = round.Id;
            dto.SessionID = session.Id;
            string oneCorrectAnswer = round.Categories.FirstOrDefault().Words.Where(a => a.StartsWith(round.Letter.ToString())).FirstOrDefault();
            dto.Answers = new List<string>() { oneCorrectAnswer, "-", "-", "-", "-" };
            return dto;
        }
    }
}
