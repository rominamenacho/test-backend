﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests.UseCase
{
    class AddAnswersToRound
    {
        IRepositorySession repositorySession;
        IRepositoryPlayer repositoryPlayer;
        CheckAnswersUseCase useCase;
        apiExample.Model.Session session;
        IRound round;
        Player player1;
        Player player2;
        AnswersCheckedDTO dtoResponse;
        IRepository<Category, int> repoCategory;
        int victoriesPlayer2BeforeUpdated;

        MapperPlayer playerMapper;
        MapperSession mapperSession;

        [SetUp]
        public void Setup()
        {
            repositorySession = new SessionRepositoryFireBase();
            repositoryPlayer = new PlayerRepositoryFireBase();
            repoCategory = new CategoryRepositoryFromFile();

            CreateEnviroment();

            player2.Victories = 5;
            victoriesPlayer2BeforeUpdated = player2.Victories;

            useCase = new CheckAnswersUseCase(repositorySession, repositoryPlayer);

            dtoResponse = useCase.Execute(CreateRoundAnswersDTOForTests(player1.Id, round));

        }

        private RoundAnswersDTO CreateRoundAnswersDTOForTests(int player1Id, IRound _round)
        {
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.PlayerID = player1Id;
            dto.RoundID = _round.Id;
            dto.SessionID = session.Id;
            string oneCorrectAnswer = _round.Categories.FirstOrDefault().Words.Where(a => a.StartsWith(_round.Letter.ToString())).FirstOrDefault();
            dto.Answers = new List<string>() { oneCorrectAnswer, "-", "-", "-", "-" };
            return dto;
        }

        private void CreateEnviroment()
        {

            session = new apiExample.Model.Session();
            round = new apiExample.Model.Round(repoCategory.GetAll());
            session.AddRoundToSession(round);

            player1 = new Player("john", "john@dsf.com");
            player2 = new Player("peter", "peter@gmail.com");
            player1.AssignSession(session);
            player2.AssignSession(session);

            playerMapper = new MapperPlayer();
            repositoryPlayer.Insert(playerMapper.FromPlayerToDbDTO(player1));
            repositoryPlayer.Insert(playerMapper.FromPlayerToDbDTO(player2));


            mapperSession = new MapperSession();
            repositorySession.Insert(mapperSession.FromSessionToDto(session));

        }



        [Test]
        public void ShouldReturnCorrectCountAnswers()
        {
            Assert.AreEqual(1, dtoResponse.CorrectCount);
        }

        [Test]
        public void ShouldReturnTrueValueForEachCorrectAnswer()
        {

            int trueBoolCount = 0;
            foreach (var answ in dtoResponse.Answers)
            {
                if (answ.Correct) trueBoolCount++;
            }

            Assert.AreEqual(dtoResponse.CorrectCount, 1);
        }

        [Test]
        public void ShouldReturnPlayerVictories()
        {
            Assert.AreEqual(player1.Victories, dtoResponse.PlayerVictories);
        }
        [Test]
        public void ShouldReturnPlayerCoins()
        {
            Assert.AreEqual(100, dtoResponse.PlayerCoins);
        }

        [Test]
        public void ShouldCreateOneTurn()
        {
            int turnsCount = repositoryPlayer.GetById(player1.Id).SessionGameList.Find(s => s.Id == session.Id).Rounds.Find(r => r.Id == round.Id).Turns.Count;
            Assert.AreEqual(1, turnsCount);
        }

        [Test]
        public void ShouldCreateTwoTurns()
        {
            AnswersCheckedDTO dtoResponse = useCase.Execute(CreateRoundAnswersDTOForTests(player2.Id, round));
            int turnsCount = repositoryPlayer.GetById(player1.Id).SessionGameList.Find(s => s.Id == session.Id).Rounds.Find(r => r.Id == round.Id).Turns.Count;

            Assert.AreEqual(2, turnsCount);
        }

        [Test]
        public void ShouldntCreateMoreThanTwoTurnsInSameRound()
        {
            AnswersCheckedDTO dtoResponse = useCase.Execute(CreateRoundAnswersDTOForTests(player2.Id, round));
            AnswersCheckedDTO dtoResponse2 = useCase.Execute(CreateRoundAnswersDTOForTests(player1.Id, round));
            AnswersCheckedDTO dtoResponse3 = useCase.Execute(CreateRoundAnswersDTOForTests(player2.Id, round));

            int turnsCount = repositoryPlayer.GetById(player1.Id).SessionGameList.Find(s => s.Id == session.Id).Rounds.Find(r => r.Id == round.Id).Turns.Count;

            Assert.AreEqual(2, turnsCount);
        }

        [Test]
        public void AfterCheckedAnswersFromBothPlayersRoundSholdBeFinished()
        {
            AnswersCheckedDTO dtoResponse = useCase.Execute(CreateRoundAnswersDTOForTests(player2.Id, round));

            bool isFinish = repositoryPlayer.GetById(player2.Id).SessionGameList.Find(s => s.Id == session.Id).Rounds.Find(r => r.Id == round.Id).Turns.LastOrDefault().IsFinished;

            Assert.IsTrue(isFinish);
        }

        [Test]
        public void AfterCheckedAnswersFromBothPlayersShouldSaveWinnerOfRound()
        {

            AnswersCheckedDTO dtoResponse = useCase.Execute(CreateDTOPlayer2Winner(round));

            int idWinner = repositoryPlayer.GetById(player2.Id).SessionGameList.Find(s => s.Id == session.Id).Rounds.Find(r => r.Id == round.Id).IdWinnerOfRound;

            Assert.AreEqual(player2.Id, idWinner);

        }

        private RoundAnswersDTO CreateDTOPlayer2Winner(IRound _round)
        {
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.PlayerID = player2.Id;
            dto.RoundID = _round.Id;
            dto.SessionID = session.Id;
            string oneCorrectAnswer = _round.Categories.FirstOrDefault().Words.Where(a => a.StartsWith(_round.Letter.ToString())).FirstOrDefault();
            string TwoCorrectAnswer = _round.Categories[1].Words.Where(a => a.StartsWith(_round.Letter.ToString())).FirstOrDefault();
            string ThreeCorrectAnswer = _round.Categories[2].Words.Where(a => a.StartsWith(_round.Letter.ToString())).FirstOrDefault();
            dto.Answers = new List<string>() { oneCorrectAnswer, TwoCorrectAnswer, ThreeCorrectAnswer, "-", "-" };
            return dto;
        }
    }
}
