﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NUnit.Framework;

namespace Tests.UseCase
{
    class FindRound
    {
        IRepositorySession repositorySession;
        FindRoundUseCase usecase;
        Session session;
        IRound round;
        MapperSession mapperSession;

        [SetUp]
        public void Setup()
        {
            repositorySession = new SessionRepositoryInMemory();
            mapperSession = new MapperSession();
            CreateEnviromentToTest();
            usecase = new FindRoundUseCase(repositorySession);
        }

        private void CreateEnviromentToTest()
        {
            IRepositoryPlayer repositoryPlayer = new PlayerRepositoryInMemory();

            IRepository<Category, int> repoCategory = new CategoryRepositoryFromFile();

            session = new Session();
            round = new apiExample.Model.Round(repoCategory.GetAll());
            session.AddRoundToSession(round);
            repositorySession.Insert(mapperSession.FromSessionToDto(session));
        }

        [Test]
        public void ShouldReturnAValidRound()
        {

            RoundDbDTO roundFinded = usecase.FindByIdAndBySessionId(round.Id, session.Id);

            Assert.IsTrue(roundFinded != null);
            Assert.AreEqual(roundFinded.Id, round.Id);
        }


    }
}
