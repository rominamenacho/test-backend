﻿using apiExample.DTO;
using apiExample.Model;
using apiExample.Repository;
using apiExample.Services;
using apiExample.UseCase;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests.UseCase
{
    public class UpdateSessionOnRepositoryTest
    {
        UpdateSessionOnRepositoryUseCase useCase;
        IRepositorySession _repositorySession;
        IRepository<Category, int> repoCategory;
        apiExample.Model.Session session;
        IRound _round1;
        MapperPlayer playerMapper;
        [SetUp]
        public void SetUp()
        {
            _repositorySession = new SessionRepositoryInMemory();
            useCase = new UpdateSessionOnRepositoryUseCase(_repositorySession);
            repoCategory = new CategoryRepositoryFromFile();
            PrepareEnviroment();
            playerMapper = new MapperPlayer();
        }

        [Test]
        public void ShouldBeenUpdatedAListOfTurnsInSpecificRound()
        {
            var countTurnsBeaforeUpdate = session.Rounds.FirstOrDefault().Turns.Count;
            SetDataToTurn();
            useCase.UpdateSessionProperties(session, _round1);

            Assert.AreNotEqual(countTurnsBeaforeUpdate, _round1.Turns.Count);
        }

        private void PrepareEnviroment()
        {
            session = new apiExample.Model.Session();

            _round1 = new Round(repoCategory.GetAll());
            session.AddRoundToSession(_round1);
            MapperSession mapperSession = new MapperSession();
            _repositorySession.Insert(mapperSession.FromSessionToDto(session));
        }

        private void SetDataToTurn()
        {
            Player player1 = new Player("john", "john@dsf.com");
            player1.AssignSession(session);
            RoundAnswersDTO dto = new RoundAnswersDTO();
            dto.PlayerID = player1.Id;
            dto.RoundID = _round1.Id;
            dto.SessionID = session.Id;
            string oneCorrectAnswer = _round1.Categories.FirstOrDefault().Words.Where(a => a.StartsWith(_round1.Letter.ToString())).FirstOrDefault();
            dto.Answers = new List<string>() { oneCorrectAnswer, "-", "-", "-", "-" };

            _round1.SetDataToTurn(dto);
        }
    }
}
